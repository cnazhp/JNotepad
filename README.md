# JNotepad - Java Notepad
## 简介
一款用Java(Swing)实现的仿Windows记事本，具备其外观和功能上以假乱真，而且还增加了Java赋予的独特的外观切换功能。
## 特性
- LAF
LAF(Look And Feel)，外观或皮肤，是Swing一种独特的技术，允许自定义外观。本应用也可以自定义外观，目前支持的有：
Windows、Metal、Nimbus等，本应用默认采用系统外观，通过JNotepad.LAF的值可以自定义。
- 国际化
自动根据客户端的Local加载不同的国际化资源
## 调用
该应用支持命令行调用和Java调用两种方式。
- 命令行  
`java -jar jnotepad-xxx.jar [参数]`
- Java
```
JNotepad notepad = new JNotepad();
// 操作
notepad.open(new File("a_file"));
// ...
// 显示
notepad.setVisible(true);
// 关闭
notepad.exit();
```
## 下载
#### 最新版  
[jnotepad-0.0.1.RELEASE](http://www.baidu.com)
#### 自主构建  
```
// 克隆到本地
git clone http://github.com/jycggyh/JNotepad.git

cd JNotepad
// 构建
gradlew build
```
